package com.iamdigger.web.service;

import com.iamdigger.web.domain.Book;

import java.util.List;


public interface BookService {

	List<Book> findBooks() throws Exception;

}