package com.iamdigger.web.controller;

import com.iamdigger.web.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Map;

/**
 * For httl testing
 * Created by Sam on 4/16/16.
 */
@Controller
public class BooksController {

    @Resource
    BookService bookService;

    @RequestMapping(value = "/books")
    public String books(Map<String, Object> context) throws Exception {
        context.put("books", bookService.findBooks());
        return "books";
    }
}
