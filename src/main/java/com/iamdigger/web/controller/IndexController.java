package com.iamdigger.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Index page controller
 * Created by Sam on 4/16/16.
 */
@Controller
public class IndexController {

    @RequestMapping(value = "/")
    public String index(HttpServletRequest request, Map<String, Object> context) throws Exception {
        context.put("contextPath", request.getContextPath());
        return "index";
    }
}
