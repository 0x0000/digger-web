package com.iamdigger.web.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 静态文件的版本控制, 将公有文件路径以及版本信息合成之后放在这里.
 *
 * Created by Sam on 4/16/16.
 */
public class ResourceVersionInterceptor extends HandlerInterceptorAdapter{

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        List<String> jss = new ArrayList<String>();
        jss.add("/static/js/jquery-2.2.3.js");
        modelAndView.addObject("jss", jss);
        super.postHandle(request, response, handler, modelAndView);
    }
}
